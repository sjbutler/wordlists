* wordlists

Wordlists provides a simple set of Java classes to make lists of words available to applications. 


** Licence

wordlists is released under the terms of the GNU Public Licence (GPL) v3 with 
the 'classpath' exception. See the files COPYING and LICENSE for details.

** Requirements
wordlists requires Java v8 or greater.  


** Wordlists

The library contains word lists derived from the SCOWL word lists
(http://wordlist.aspell.net/) for en_GB, en_US, and en_CA that are used as an input to aspell. 
Lists of abbreviations and technical terms are provided. 
Lists of abbreviations derived from Emily Hill's AMAP project are 
included (original is currently available from http://cs.drew.edu/~emhill/AMAP.tar.gz, and more detail of the original study is preserved at http://cs.drew.edu/~emhill/ud/amap/). 
A German word list is also included. 

** Usage

The ~Wordlists~ enumeration contains constants representing the wordlists available in the library that are used as arguments to the ~Wordlist~ constructors to create a wordlist. Details of the files incorporated into each of the wordlists identified by the enum can be found in the docs folder. 

